# Identifying routes from paths

Setup & Data
Briefly, individually marked bees had to cross a cluttered tunnel in order to return home. The time courses of the bees' position were used to determine different routes in the clutter.

![](clutter.png) 

## Chapter 1 | Preprocessing of the trajectories for clustering

In the first chapter, the timecourses of the bumblebees is resampled with a fixed step length from 2mm/frame to 11mm/frame.

## Chapter 2 | Using similarity measures to identify cluster

The timecourses of the bumblebees are compared by using two similarity measures: Fréchet and DTW. These measures are fed to a clustering algorithm (M3C) to identify cluster. The chapter ends with the trajectories colour coded with their belonging cluster.

## Chapter 3 | Using flight characteristics to identify cluster

The timecourses of the bumblebees are described with seven characteristics. These measures are fed to a clustering algorithm (M3C) to identify cluster. The chapter ends with the trajectories colour coded with their belonging cluster.

## Chapter 4 | Effect of the preprocessing on clustering

We look at a potential change in the clustering results by varying the resampling step length.