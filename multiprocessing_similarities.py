import pandas as pd
import numpy as np
import os
import socket
import sys
import argparse
# Instructions on how to install navipy can be found in the README file.
from navipy.trajectories.similarity import pairwise_comp
from navipy.trajectories.similarity.dtw.multi import multi_dtw
from navipy.trajectories.similarity.twed.multi import multi_twed
from navipy.trajectories.similarity.frechet.multi import multi_frechet

parser = argparse.ArgumentParser()
msg = "method names: \n"
msg += "* dtw\n"
msg += "* frechet\n"
parser.add_argument("--method", help=msg, type=str)
args = parser.parse_args()


hdf_file = 'data_all.hdf' # path to the data file
conditions = pd.read_hdf(hdf_file, key='conditions') # read conditions-table from hdf file
traj_dates = conditions['Filename'] # get the dates for the trajectories from the conditions-table
# we will load the trajectory with the dates and the following prefix as key from the hdf file
key_template = 'traj_coeff_{}' # will be used as: key_template.format(date)


traj_dict = {}
for date in traj_dates:
    cur_key = key_template.format(date)
    traj = pd.read_hdf(hdf_file, key=cur_key)
    
    # for our result, we only use x and y coordinates
    traj = traj.loc[:, ['x', 'y']].astype(float) 
    
    # the pairwise_comp function needs a dictionary,
    # where the key is the key as in the hdf file,
    # and the item is the trajectory (Pandas DataFrame)
    traj_dict[cur_key] = traj
    

if __name__ ==  '__main__':

            
    if args.method == 'frechet':
        results = pairwise_comp(traj_dict, func=multi_frechet)
        results = results.astype(float)
        results.to_hdf('results/similarityMeasures/distance_results_raw.hdf', key='frechet_coeff')

        
    elif args.method == 'dtw':
        results = pairwise_comp(traj_dict, func=multi_dtw)
        results = results.astype(float)
        results.to_hdf('results/similarityMeasures/distance_results_raw.hdf', key='dtw_coeff')
        
        
    else:
        msg = '{} is not supported'.format(args.method)
        raise KeyError(msg)
