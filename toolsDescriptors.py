import numpy as np
import pandas as pd
import math


def get_AvgLatPos(traj):
    avg_mean = np.mean(traj.y)
    #avg_median = np.median(traj.y)
    
    return avg_mean

def get_MaxMinLatPos(traj):
    maxPos = np.max(traj.y)
    minPos = np.min(traj.y)
    
    return maxPos, minPos


def get_StdLatPos(traj):
    std = np.std(traj.y)
    return std



def get_AvgStdAngle(traj):
    angles = []
    for i in traj.index:
        rad = math.atan2(traj.x[i], traj.y[i])
        deg = math.degrees(rad)
        direction = 90 
        deg = abs(deg)
        deg = deg - direction # correct for y-direction
        deg = abs(deg)
        angles.append(deg)
        
    angles_std = np.std(angles)    
    angles_mean = np.mean(angles)
    angles_cum = np.sum(angles)
    
    return angles_mean, angles_std, angles_cum
    
    

def get_AvgSpeed(traj, fps=1):
    """ Get speed
    assume that traj as x,y
    """
    contraj = traj.copy() 
    contraj = contraj.astype(float)
    speed = np.sqrt(contraj.x.diff()**2+contraj.y.diff()**2) # distance / frame
    speed = speed*fps
    speed = np.nanmean(speed)
    
    return speed

def get_sinuosity(traj):
        
    contraj = traj.copy() 
    contraj = contraj.astype(float)
    travel_dist = np.nansum(np.sqrt(contraj.x.diff()**2+contraj.y.diff()**2)) # sum of distance / frame
    
    shortest_dist = np.sqrt((contraj.x.iloc[-1]-contraj.x.iloc[0])**2 + (contraj.y.iloc[-1]-contraj.y.iloc[0])**2)
 
    sinuosity = travel_dist / shortest_dist
    
    return sinuosity